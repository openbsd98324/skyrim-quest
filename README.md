# skyrim-quest


 ## S001-Shrine of Talos: Froki's Peak




From Autumnwatch Tower, head east up the mountain side and past Froki's Shack, and eventually an unmarked Shrine of Talos will be found.



![](https://gitlab.com/openbsd98324/skyrim-quest/-/raw/main/S001-Shrine%20of%20Talos:%20Froki's%20Peak/Froki_s_Peak_Shrine_of_Talos.png)


![](https://gitlab.com/openbsd98324/skyrim-quest/-/raw/main/S001-Shrine%20of%20Talos:%20Froki's%20Peak/ShrineOfTalos.png)


## S002 Bolar's Oathblade


"A Sword of great power"

 

 https://gitlab.com/openbsd98324/skyrim-quest/-/raw/main/S002-bolar-oathblad/bolar-3.png


> Acquisition
It can be found in Bloated Man's Grotto along with his written oath, under the shrine of Talos.

> Smithing
It can be upgraded with a Quicksilver Ingot and the Arcane Blacksmith perk at a grindstone, and also benefits from the Steel Smithing perk, which doubles the improvement.



##  References

https://elderscrolls.fandom.com/wiki/Weapons_(Skyrim)#Swords

https://elderscrolls.fandom.com/wiki/Unmarked_Locations_(Skyrim)





